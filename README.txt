README.txt

Install Cue Builder as usual. 
Proceed to Configuration > Cue Builder Settings > Map Cues to Content Type. Choose the content type that you want cues to be connected to, and this will create a node reference field on the cue content type. 

Enable the 'Add Cues' block from the block admin page. 
Enable the 'Cues by Content' block from the block admin page. 

