<?php

/**
 * Cue view callback.
 */
function cue_builder_view($cue) {
  drupal_set_title(entity_label('cue', $cue));
  return entity_view('cue', array(entity_id('cue', $cue) => $cue), 'full');
}
