<?php
/**
 * @file
 * cue_builder.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function cue_builder_field_default_fields() {
  $fields = array();

  // Exported field: 'cue-cue-field_cue_collection'
  $fields['cue-cue-field_cue_collection'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_cue_collection',
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'field_collection',
      'settings' => array(
        'path' => '',
        'profile2_private' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'field_collection',
    ),
    'field_instance' => array(
      'bundle' => 'cue',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'field_collection',
          'settings' => array(
            'add' => 'Add',
            'delete' => 'Delete',
            'description' => TRUE,
            'edit' => 'Edit',
            'view_mode' => 'full',
          ),
          'type' => 'field_collection_view',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'cue',
      'field_name' => 'field_cue_collection',
      'label' => 'Cue Collection',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'field_collection',
        'settings' => array(),
        'type' => 'field_collection_embed',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'field_collection_item-field_cue_collection-field_cue_description'
  $fields['field_collection_item-field_cue_collection-field_cue_description'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_cue_description',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'module' => 'text',
      'settings' => array(
        'profile2_private' => FALSE,
      ),
      'translatable' => '0',
      'type' => 'text_long',
    ),
    'field_instance' => array(
      'bundle' => 'field_cue_collection',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'field_collection_item',
      'field_name' => 'field_cue_description',
      'label' => 'Description',
      'required' => 1,
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '1',
        ),
        'type' => 'text_textarea',
        'weight' => '2',
      ),
    ),
  );

  // Exported field: 'field_collection_item-field_cue_collection-field_cue_endtime'
  $fields['field_collection_item-field_cue_collection-field_cue_endtime'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_cue_endtime',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'hms_field',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'hms_field',
    ),
    'field_instance' => array(
      'bundle' => 'field_cue_collection',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'hms_field',
          'settings' => array(
            'format' => 'h:mm',
            'leading_zero' => TRUE,
          ),
          'type' => 'hms_default_formatter',
          'weight' => 4,
        ),
      ),
      'entity_type' => 'field_collection_item',
      'field_name' => 'field_cue_endtime',
      'label' => 'Cue End',
      'required' => 0,
      'settings' => array(
        'format' => 'h:mm:ss',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'hms_field',
        'settings' => array(),
        'type' => 'hms_default_widget',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'field_collection_item-field_cue_collection-field_cue_starttime'
  $fields['field_collection_item-field_cue_collection-field_cue_starttime'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_cue_starttime',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'module' => 'hms_field',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'hms_field',
    ),
    'field_instance' => array(
      'bundle' => 'field_cue_collection',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'hms_field',
          'settings' => array(
            'format' => 'h:mm',
            'leading_zero' => TRUE,
          ),
          'type' => 'hms_default_formatter',
          'weight' => 3,
        ),
      ),
      'entity_type' => 'field_collection_item',
      'field_name' => 'field_cue_starttime',
      'label' => 'Cue Start',
      'required' => 1,
      'settings' => array(
        'format' => 'h:mm:ss',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'hms_field',
        'settings' => array(),
        'type' => 'hms_default_widget',
        'weight' => '0',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Cue Collection');
  t('Cue End');
  t('Cue Start');
  t('Description');

  return $fields;
}