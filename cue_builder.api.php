<?php
/**
* @file
* Hooks provided by this module.
*/

/**
* @addtogroup hooks
* @{
*/

/**
* Acts on {ENTITIES} being loaded from the database.
*
* This hook is invoked during {ENTITY} loading, which is handled by
* entity_load(), via the EntityCRUDController.
*
* @param array {$ENTITIES}
*   An array of {ENTITY} entities being loaded, keyed by id.
*
* @see hook_entity_load()
*/
function hook_cue_builder_load(array $cues) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
* Responds when a {ENTITY} is inserted.
*
* This hook is invoked after the {ENTITY} is inserted into the database.
*
* @param {ENTITY_CLASS} {$ENTITY}
*   The {ENTITY} that is being inserted.
*
* @see hook_entity_insert()
*/
function hook_cue_builder_insert(Cue $cue) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('cue_builder', $cue),
      'extra' => print_r($cue, TRUE),
    ))
    ->execute();
}

/**
* Acts on a {ENTITY} being inserted or updated.
*
* This hook is invoked before the {ENTITY} is saved to the database.
*
* @param {ENTITY_CLASS} {$ENTITY}
*   The {ENTITY} that is being inserted or updated.
*
* @see hook_entity_presave()
*/
function hook_cue_builder_presave(Cue $cue) {
  $cue->name = 'foo';
}

/**
* Responds to a {ENTITY} being updated.
*
* This hook is invoked after the {ENTITY} has been updated in the database.
*
* @param {ENTITY_CLASS} {$ENTITY}
*   The {ENTITY} that is being updated.
*
* @see hook_entity_update()
*/
function hook_cue_builder_update(Cue $cue) {
  db_update('mytable')
    ->fields(array('extra' => print_r($cue, TRUE)))
    ->condition('id', entity_id('cue_builder', $cue))
    ->execute();
}

/**
* Responds to {ENTITY} deletion.
*
* This hook is invoked after the {ENTITY} has been removed from the database.
*
* @param {ENTITY_CLASS} {$ENTITY}
*   The {ENTITY} that is being deleted.
*
* @see hook_entity_delete()
*/
function hook_cue_builder_delete(Cue $cue) {
  db_delete('mytable')
    ->condition('pid', entity_id('cue_builder', $cue))
    ->execute();
}

/**
* @}
*/