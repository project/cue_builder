<?php 

/**
 * @file
 * Defines administration pages for the cue builder module.
 */

/**
 * Provide info for the entity administration page. 
 */
function cue_builder_settings_page() {
  $output = '<ul class="admin-list"><li>';
  $output .= l('View All Cues', 'admin/content/cue');
  $output .= '</li></ul>';
 
  $output .= '<ul class="admin-list"><li>';
  $output .= l('Add Cues', 'cue/add');
  $output .= '</li></ul>';
 
  $output .= '<ul class="admin-list"><li>';
  $output .= l('Manage Cue Structure', 'admin/structure/cue-builder/manage/fields');
  $output .= '</li></ul>';
  
  $output .= '<ul class="admin-list"><li>';
  $output .= l('Delete All Cues', 'cue/delete/all');
  $output .= '</li></ul>';

  $output .= '<ul class="admin-list"><li>';
  $output .= l('Map Cues to Content Type', 'cue/map/type');
  $output .= '</li></ul>';  
  
  $view = views_get_view('cues');
  $output .= $view->execute_display('default');

  return $output;

}


/**
 * Form to map a content type to cue entity.
 */
function cue_builder_types_form($form, &$form_state) {
 $types = node_type_get_types();   
 $options = array(0 => t('<None>'));
 foreach ($types as $type) {
   $options[$type->type] = $type->name;
 }
 
 $form['type'] = array(
   '#type' => 'select',
   '#title' => t('Content Type to Attach Cues'),
   '#description' => t('Attach a specific content type to cues (creates a node reference field)'),
   '#options' => $options,
   '#default_value' => variable_get('cue_builder_type', ''),
 );

   
  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Map'),
    '#submit' => $submit + array('cue_builder_types_form_submit'),
  );
 //debug($types);
  
  return $form;
}

/**
 * Submit handler for mapping a content type to cue entity. 
 */
function cue_builder_types_form_submit($form, &$form_state){
  //debug($form);
  if(variable_get('cue_builder_type', '') != $form['type']['#value']){
  
  // Delete the content reference field for cues.
    field_delete_field('field_cue_'.variable_get('cue_builder_type', '').'_ref');
  

  // Delete the content reference instance for cues.
    field_delete_instance('field_cue_'.variable_get('cue_builder_type', '').'_ref');
  	
  	
  	variable_set('cue_builder_type', $form['type']['#value']);
  	
  // Create the content reference field for cues.
  foreach (_cue_builder_type_add_reference($form['type']['#value']) as $field) {
    field_create_field($field);
    variable_set('cue_builder_ref_field', $field['field_name']);
  }

  // Create the content reference instance for cues.
  foreach (_cue_builder_installed_instances($form['type']['#value']) as $instance) {
    $instance['entity_type'] = 'cue';
    $instance['bundle'] = 'cue';
    field_create_instance($instance);
  }
        
  drupal_set_message('The cue builder content type has been set. A node reference field has been created to connect Cue Builder to this content type.', 'status');
  
  }

  $form_state['redirect'] = 'cue/map/type';
}

function _cue_builder_type_add_reference($type) {
  $t = get_t();
  
  // Load the content type we are going to reference
  $reference_type = node_type_get_type($type);
  
  $fields = array(
      // node refererence auto complete field (see the instance), referencing a content-type called 'work'
    $type.'_ref' => array(
      'field_name'  => 'field_cue_'.$type.'_ref',
      'label'       => $t($reference_type->name),
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'type'        => 'node_reference',
      'settings'    => array(
        'referenceable_types'  => array($type),
      ),
    ),
  );
  return $fields;
}

function _cue_builder_installed_instances($type) {
  $t = get_t();
  
  // Load the content type we are going to reference
  $reference_type = node_type_get_type($type);
  
  $instances = array(
      // instance of the node reference 'work' auto complete field above
    $type.'_ref' => array(
      'field_name'  => 'field_cue_'.$type.'_ref',
      'label'       => $t($reference_type->name),
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'widget'      => array(
        'type'          => 'node_reference_autocomplete',
      ),
    ),
  );
  return $instances;
}