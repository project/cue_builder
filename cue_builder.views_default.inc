<?php

/**
 * Implements hook_views_default_views().
 */
function cue_builder_views_default_views() {

 $view = new view;
$view->name = 'cues';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'cue_builder';
$view->human_name = 'cues';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'cues';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['style_plugin'] = 'table';
/* Field: Cue: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'cue_builder';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
/* Field: Cue: Show */
$handler->display->display_options['fields']['field_cue_show_reference']['id'] = 'field_cue_show_reference';
$handler->display->display_options['fields']['field_cue_show_reference']['table'] = 'field_data_field_cue_show_reference';
$handler->display->display_options['fields']['field_cue_show_reference']['field'] = 'field_cue_show_reference';
$handler->display->display_options['fields']['field_cue_show_reference']['label'] = '';
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_cue_show_reference']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_cue_show_reference']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_cue_show_reference']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_cue_show_reference']['field_api_classes'] = 0;
/* Field: Cue: Cue ID */
$handler->display->display_options['fields']['cue_id']['id'] = 'cue_id';
$handler->display->display_options['fields']['cue_id']['table'] = 'cue_builder';
$handler->display->display_options['fields']['cue_id']['field'] = 'cue_id';
$handler->display->display_options['fields']['cue_id']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['external'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['trim'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['html'] = 0;
$handler->display->display_options['fields']['cue_id']['element_label_colon'] = 1;
$handler->display->display_options['fields']['cue_id']['element_default_classes'] = 1;
$handler->display->display_options['fields']['cue_id']['hide_empty'] = 0;
$handler->display->display_options['fields']['cue_id']['empty_zero'] = 0;
$handler->display->display_options['fields']['cue_id']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['cue_id']['format_plural'] = 0;
/* Sort criterion: Cue: Cue Collection (field_cue_collection:delta) */
$handler->display->display_options['sorts']['delta']['id'] = 'delta';
$handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_cue_collection';
$handler->display->display_options['sorts']['delta']['field'] = 'delta';

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'cues';

/* Display: Cue Block */
$handler = $view->new_display('block', 'Cue Block', 'cues_by_content');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['display_description'] = 'Displays per content cues';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
$handler->display->display_options['row_options']['inline'] = array(
  'title' => 'title',
  'field_cue_show_reference' => 'field_cue_show_reference',
);
$handler->display->display_options['row_options']['hide_empty'] = 0;
$handler->display->display_options['row_options']['default_field_elements'] = 1;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Cue: Cue ID */
$handler->display->display_options['fields']['cue_id']['id'] = 'cue_id';
$handler->display->display_options['fields']['cue_id']['table'] = 'cue_builder';
$handler->display->display_options['fields']['cue_id']['field'] = 'cue_id';
$handler->display->display_options['fields']['cue_id']['label'] = '';
$handler->display->display_options['fields']['cue_id']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['text'] = 'Edit all Cues';
$handler->display->display_options['fields']['cue_id']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['path'] = 'cue/[cue_id]/edit';
$handler->display->display_options['fields']['cue_id']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['external'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['alt'] = 'Edit Cue';
$handler->display->display_options['fields']['cue_id']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['cue_id']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['trim'] = 0;
$handler->display->display_options['fields']['cue_id']['alter']['html'] = 0;
$handler->display->display_options['fields']['cue_id']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['cue_id']['element_default_classes'] = 1;
$handler->display->display_options['fields']['cue_id']['hide_empty'] = 0;
$handler->display->display_options['fields']['cue_id']['empty_zero'] = 0;
$handler->display->display_options['fields']['cue_id']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['cue_id']['format_plural'] = 0;
/* Field: Cue: Cue Collection */
$handler->display->display_options['fields']['field_cue_collection']['id'] = 'field_cue_collection';
$handler->display->display_options['fields']['field_cue_collection']['table'] = 'field_data_field_cue_collection';
$handler->display->display_options['fields']['field_cue_collection']['field'] = 'field_cue_collection';
$handler->display->display_options['fields']['field_cue_collection']['label'] = '';
$handler->display->display_options['fields']['field_cue_collection']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['external'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['field_cue_collection']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['field_cue_collection']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['trim'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['alter']['html'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_cue_collection']['element_default_classes'] = 1;
$handler->display->display_options['fields']['field_cue_collection']['hide_empty'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['empty_zero'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['field_cue_collection']['type'] = 'field_collection_table_view';
$handler->display->display_options['fields']['field_cue_collection']['settings'] = array(
  'edit' => 'Edit',
  'delete' => 'Delete',
  'add' => 'Add',
  'description' => 1,
  'view_mode' => 'full',
  'empty' => 1,
);
$handler->display->display_options['fields']['field_cue_collection']['group_rows'] = 1;
$handler->display->display_options['fields']['field_cue_collection']['delta_offset'] = '0';
$handler->display->display_options['fields']['field_cue_collection']['delta_reversed'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['delta_first_last'] = 0;
$handler->display->display_options['fields']['field_cue_collection']['multi_type'] = 'ul';
$handler->display->display_options['fields']['field_cue_collection']['field_api_classes'] = 0;
$handler->display->display_options['defaults']['sorts'] = FALSE;
$handler->display->display_options['defaults']['arguments'] = FALSE;
/* Contextual filter: Cue: Show (field_cue_show_reference) */
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['id'] = 'field_cue_show_reference_nid';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['table'] = 'field_data_field_cue_show_reference';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['field'] = 'field_cue_show_reference_nid';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['field_cue_show_reference_nid']['not'] = 0;
$handler->display->display_options['block_description'] = 'Cues by Content';

  $views['cues'] = $view;
  
$view1 = new view;
$view1->name = 'add_cues';
$view1->description = '';
$view1->tag = 'default';
$view1->base_table = 'node';
$view1->human_name = 'Add Cues';
$view1->core = 7;
$view1->api_version = '3.0';
$view1->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view1->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Cue Builder';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'create cue_builder entities';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '5';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['exclude'] = TRUE;
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['external'] = 0;
$handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title']['element_default_classes'] = 1;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['title']['link_to_node'] = 0;
/* Field: Content: Nid */
$handler->display->display_options['fields']['nid']['id'] = 'nid';
$handler->display->display_options['fields']['nid']['table'] = 'node';
$handler->display->display_options['fields']['nid']['field'] = 'nid';
$handler->display->display_options['fields']['nid']['label'] = '';
$handler->display->display_options['fields']['nid']['exclude'] = TRUE;
$handler->display->display_options['fields']['nid']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['nid']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nid']['alter']['external'] = 0;
$handler->display->display_options['fields']['nid']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nid']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nid']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nid']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nid']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['nid']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nid']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nid']['alter']['html'] = 0;
$handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nid']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nid']['hide_empty'] = 0;
$handler->display->display_options['fields']['nid']['empty_zero'] = 0;
$handler->display->display_options['fields']['nid']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['nid']['link_to_node'] = 0;
/* Field: Global: Custom text */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = 'Add Cues for [title]';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['path'] = 'cue/add/[nid]';
$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Contextual filter: Content: Nid */
$handler->display->display_options['arguments']['nid']['id'] = 'nid';
$handler->display->display_options['arguments']['nid']['table'] = 'node';
$handler->display->display_options['arguments']['nid']['field'] = 'nid';
$handler->display->display_options['arguments']['nid']['default_action'] = 'default';
$handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
$handler->display->display_options['arguments']['nid']['default_argument_skip_url'] = 0;
$handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['nid']['break_phrase'] = 0;
$handler->display->display_options['arguments']['nid']['not'] = 0;
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

/* Display: Block */
$handler = $view1->new_display('block', 'Block', 'block');

  $views['cueadd'] = $view1;

  return $views;
}